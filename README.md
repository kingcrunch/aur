# AUR repositories

| Name                                                                                            | Releases                                                                      |
|-------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| [`alertmanager-bin`](https://aur.archlinux.org/packages/alertmanager-bin)                       | [0.28.0](https://github.com/prometheus/alertmanager/releases/tag/v0.28.0)     |
| [`dephpend`](https://aur.archlinux.org/packages/dephpend)                                       | [0.8.0](https://github.com/mihaeu/dephpend/releases/tag/0.8.0)                |
| [`kaniko`](https://aur.archlinux.org/packages/kaniko)                                           | [1.23.2](https://github.com/GoogleContainerTools/kaniko/releases/tag/v1.23.2) |
| [`phive`](https://aur.archlinux.org/packages/phive)                                             | [0.15.3](https://github.com/phar-io/phive/releases/tag/0.15.3)                |
| [`prometheus-bin`](https://aur.archlinux.org/packages/prometheus-bin)                           | [3.2.1](https://github.com/prometheus/prometheus/releases/tag/v3.2.1)        |
| [`prometheus-process-exporter`](https://aur.archlinux.org/packages/prometheus-process-exporter) | [0.8.5](https://github.com/ncabatoff/process-exporter/releases/tag/v0.8.5)    |
| [`opentofu-bin`](https://aur.archlinux.org/packages/opentofu-bin)                               | [1.9.0](https://github.com/opentofu/opentofu/releases/tag/v1.9.0)             |
| [`opentofu-git`](https://aur.archlinux.org/packages/opentofu-git)                               | -                                                                             |