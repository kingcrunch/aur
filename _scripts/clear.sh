#!/bin/sh
set -eou pipefail

find $PWD -type f '(' -name "*.tar.gz" -or -name "*.part" -or -name '*.zip' ')' -delete
