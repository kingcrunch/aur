#!/bin/sh
set -eou pipefail

git reset HEAD -- .
git checkout -- README.md
version=$(grep '^pkgver=' $1/PKGBUILD | cut -d'=' -f2)

sed --regexp-extended --in-place --expression="s/pkgver=.+/pkgver=$2/g" $1/PKGBUILD

(
  cd $PWD/$1
  updpkgsums PKGBUILD
  makepkg --printsrcinfo > .SRCINFO
)
sed -i -E "/$1/s/$version/$2/g" README.md

git add $1
git add README.md

echo "update from $version to $2"
echo "We staged all relevant changes. Press any key to continue, or CTRL+C to cancel"
read -p "Press to start..." -n1 -s

git commit -m "$1 v$2"
git subtree push --prefix=$1 ssh://aur@aur.archlinux.org/$1.git master
